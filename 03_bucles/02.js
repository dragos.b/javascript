/*
Web que lea una base y un exponente 
y escriba la base elevado al exponente intraducido 
(Para el exponente se tomarán siempre valores positivos incluido el 0)
*/

function calculaExp() {
	var base,
		exponente,
		res = 1

	// Entrada
	base      = document.getElementById ('base')
	exponente = document.getElementById ('exp')

	base      = parseInt ( base.value, 10 )
	exponente = parseInt ( exponente.value, 10 )

	// Calculos
	for ( i=0; i<exponente; i++ )
		res *= base

	// Salida
	out.innerHTML = base + " ^ " + exponente
	out.innerHTML += " = " + res
}