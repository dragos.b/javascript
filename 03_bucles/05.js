function darFormato() {
    var y, m, d,
        esn       = "<br/>",
        verdadero, falso

    // Año
    y = document.getElementById (  'year'  )
    m = document.getElementById (  'month' )
    d = document.getElementById (  'day'   )

    // Conversion a enteros
    y = parseInt( y.value, 10 )
    m = parseInt( m.value, 10 )
    d = parseInt( d.value, 10 )

    // Guardo la salida en variables para que no sea muy largo el operador ternario
    verdadero = "Día " + d + " de " + retornaMes(m) + " del " + y + esn
    falso     = "Fecha incorrecta"

    // Salida
    fechaCorrecta ( y, m, d ) ? out.innerHTML = verdadero : out.innerHTML = falso
}

function retornaMes ( mes ) {
    var meses =
        [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ]
    return meses [mes - 1]
}

// Comprueba si la fecha es correcta
function fechaCorrecta ( y, m , d ) {
    if ( m < 1 || m > 12 ) {
        return false
    }
    else if (   m == 1  ||
                m == 3  ||
                m == 5  ||
                m == 7  ||
                m == 8  ||
                m == 10 ||
                m == 12 
               ) {
        if ( d >= 1 && d <= 31 )
            return true
    }
    else if (   m == 4 || 
                m == 6 ||
                m == 9 ||
                m == 11 
              ) {
        if ( d>= 1 && d <= 30 )
            return true
    }
    else if ( m == 2 ) {
        if ( d >= 1 && d <= 29 && anioBisiesto( y ) )
            return true
        else if ( d >= 1 && d <= 28 )
            return true
    }
    else {
        return false
    }
}

// Calcula año bisiesto
function anioBisiesto ( y ) {
    if ( ( y % 4 ) == 0 && ( y % 100 ) != 0 || ( y % 400 ) == 0 )
        return true
    else return false
}